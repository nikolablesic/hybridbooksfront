import React from 'react'
import './Dropdown.css'

const Dropdown = () => {
  return (
    <div className="menu-nav">
    <div className="dropdown-container" tabIndex={-1}>
      <div className="three-dots"></div>
      <div className="dropdown">
        <a href="#"><div>Edit</div></a>
        <a href="#"><div>Delete</div></a>
      </div>
    </div>
  </div>
  )
}
export default Dropdown
