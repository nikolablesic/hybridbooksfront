import React from 'react'
import Dropdown from '../Dropdown/Dropdown'
import './Book.css'

const Book = () => {
  return (
      <div className='cards'>
        <div className='card'>
            <div className='book-image'>
                <img src='/assets/book-covers/book-cover01.jpeg'/>
            </div>
            <div className='book-details'>
                <Dropdown/>
                <h1>Title</h1>
                <div className='details'>
                    <b>Description: </b>Bksahdka s,d ,as dkj asjkdbhas dka, sdn akjs dja sdjla sdjk<br/>
                    <b>Authors: </b><i>Nias da SDSdm, ASDkjddh ASDSD, sajhdvjSSD asj d,as SS</i><br/>
                    Available copies: 5<b/>
                </div>
                <input className='rent-button' type='button' value='RENT'/>
            </div>
        </div>
        <div className='card'>
            <div className='book-image'>
                <img src='/assets/book-covers/book-cover01.jpeg'/>
            </div>
            <div className='book-details'>
                <Dropdown/>
                <h1>Title</h1>
                <div className='details'>
                    <b>Description: </b>Bksahdka s,d ,as dkj asjkdbhas dka, sdn akjs dja sdjla sdjk<br/>
                    <b>Authors: </b><i>Nias da SDSdm, ASDkjddh ASDSD, sajhdvjSSD asj d,as SS</i><br/>
                    Available copies: 5<b/>
                </div>
                <input className='rent-button' type='button' value='RENT'/>
            </div>
        </div>
      </div>
  )
}
export default Book
